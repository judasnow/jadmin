;;

(import tornado.ioloop)
(import tornado.web)

(defclass MainHandler [tornado.web.RequestHandler]
    [[_get
        (fn [self] (.write self "hello hy"))]])


(setv MainHandler.get MainHandler._get)


(setv application
    (tornado.web.Application
        ['("/" MainHandler)]
        {"debug" True}))

(if (= __name__ "__main__")
  (do
    (application.listen 8888)
    ((. (tornado.ioloop.IOLoop.instance) start))))


