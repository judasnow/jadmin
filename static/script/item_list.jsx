define(["react"], function(React) {
"use strict";


var Tr = React.createClass({
    render: function() {
        return (
            <tr></tr>
        )
    }
});

var Th = React.createClass({
    render: function() {
        return (
            <th>{this.props.text}</th>
        )
    }
});

var Td = React.createClass({
    render: function() {
        return (
            <td>{this.props.text}</td>
        )
    }
});

var ItemList = React.createClass({
    render: function() {
        return (
            <table className="item-list">
                <Tr>
                    <Th></Th>
                </Tr>
            </table>
        );
    }
});

React.render(<ItemList />, document.getElementById("content"));

});


