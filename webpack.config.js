
module.exports = {
    entry: "./static/script/entry.js",
    output: {
        path: __dirname,
        filename: "bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                loader: "style!css"
            },
            {
                test: /\.less$/,
                loader: "style!css!less"
            },
            {
                test: /\.jsx$/,
                loader: "jsx-loader"
            }
        ]
    }
};

